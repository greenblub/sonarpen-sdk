# SonarPen Wrapper



## Project structure

- arr: sonarpen sdk android archive
- cpp: c++ Wrapper which invoke sonarpen sdk's functions
- java: jni interface, which is called from Qt android library


## Build krita with sonarpen sdk (using Docker)

### Prerequisites
    
[Refer Prerequisites section of this guide](https://docs.krita.org/en/untranslatable_pages/building/build_krita_with_docker_on_linux.html#prerequisites)


### Downloading Krita sources
    # create directory structure for container control directory
    git clone https://invent.kde.org/greenblub/krita-docker-env krita-auto-1

    cd krita-auto-1
    mkdir persistent
    git clone https://invent.kde.org/greenblub/krita.git krita -b krita/5.2.2.x

### Fetch CI-management repositories

    pushd ./persistent/krita
    git clone https://invent.kde.org/greenblub/krita-deps-management.git
    git clone https://invent.kde.org/greenblub/ci-utilities.git krita-deps-management/ci-utilities
    popd

### Building the Android container
    ./bin/bootstrap-krita-deps.sh --android=arm64-v8a
    # you can change target architecture: x86_64, armeabi-v7a or arm64-v8a

    ./bin/build_image --android
    ./bin/run_container

### Enter the container and build Krita
    # enter the container
    ./bin/enter

    # set up enviroment
    # set ABI you are building for
    export KDECI_ANDROID_ABI=arm64-v8a

    # location where _build and _packaging folders will be located
    # (don't change)
    export KDECI_WORKDIR_PATH=/home/appimage/appimage-workspace

    # location where the dependencies were unpacked (don't change)
    export KDECI_SHARED_INSTALL_PATH=/home/appimage/appimage-workspace/deps/usr

    mkdir -p /home/appimage/appimage-workspace/krita/_build
    cd /home/appimage/appimage-workspace/krita/_build

#### Configure Krita:

    cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo \
      -DHIDE_SAFE_ASSERTS=OFF \
      -DBUILD_TESTING=OFF \
      -DCMAKE_INSTALL_PREFIX=~/appimage-workspace/deps/usr/ \
      -DCMAKE_TOOLCHAIN_FILE=~/persistent/krita/krita-deps-management/tools/android-toolchain-krita.cmake \
      -DANDROID_ENABLE_STDIO_FORWARDING=ON \
      ~/persistent/krita/

#### Build krita libs
    make -j8 install

#### Building the APK package
    python ~/persistent/krita/build-tools/ci-scripts/build-android-package.py
    
And you will get an APK package in $KDECI_WORKDIR_PATH/krita/_packaging





